// Importar los módulos necesarios
const http = require("http");
const express = require("express");
const bodyparser = require("body-parser");
const cors = require('cors');
const path = require("path");

// Crear una instancia de Express
const app = express();

// Configuración de Express
app.use(express.json());
app.use(cors());
app.set("view engine","ejs");
app.use(express.static(__dirname + "/public"));
app.use(bodyparser.urlencoded({ extended: true }));
app.engine("html", require("ejs").renderFile);
const misRutas = require("./router/index");
app.use(misRutas);

// Agregar un middleware para manejar solicitudes no encontradas (404)
app.use((req, res, next) => {
  res.status(404).sendFile(__dirname + "/public/error.html");
});

// Iniciar el servidor en el puerto 3030 y mostrar un mensaje en la consola
const puerto = 3030;
app.listen(puerto, () => {
  console.log("Iniciando Puerto 3030");
});
