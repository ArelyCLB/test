// Importar el módulo body-parser y el framework Express
const bodyParser = require("body-parser");
const express = require("express");

// Crear una instancia de Router
const router = express.Router();

const AlumnosDb = require('../models/alumnos.js');

alumno = {}

//Ejemplo para insertar lo dio el profe
router.post("/insertar", async(req, res) => {
  // Se obtienen los valores enviados en el formulario
  const alumno = {
    matricula: req.body.matricula,
    nombre: req.body.nombre,
    domicilio: req.body.domicilio,
    sexo: req.body.sexo,
    especialidad: req.body.especialidad
  };

  resultado = await AlumnosDb.insertar(alumno);
  res.json(resultado);
});

//Ejemplo para eliminar lo dio el profe
router.post("/eliminar", async(req, res) => {
  var matricula = req.body.matricula;

  resultado = await AlumnosDb.borrar(matricula);
  res.json(resultado);
});


router.post("/actualizar", async (req, res) => {
  const matricula = req.body.matricula;
  const alumno = {
    nombre: req.body.nombre,
    domicilio: req.body.domicilio,
    sexo: req.body.sexo,
    especialidad: req.body.especialidad
  }
  
  // Se llama al método 'actualizar' de la base de datos y se devuelve una respuesta
  resultado = await AlumnosDb.actualizar(matricula, alumno);
  res.json(resultado);
});


router.post("/consultarXmatricula", async(req, res) => {
  // Se obtienen los valores enviados en el formulario
  var matricula = req.body.matricula;

  resultado = await AlumnosDb.buscarXMatricula(matricula);
  res.json(resultado);
});

// Ruta principal que muestra una página con los datos del array "datos"
router.get('/mostrarTodos', async (req, res) => {
  try {
    resultado = await AlumnosDb.mostrarTodos();
    res.json(resultado);
  } catch (error) {
    console.error(error);
    res.status(500).send('Error al cargar la lista de alumnos.');
  }
});

// Exportar el módulo Router para que pueda ser utilizado en otros archivos
module.exports = router;