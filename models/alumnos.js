// Se importa el módulo 'response' de 'express'
const json = require('express/lib/response');

// Se importa el módulo de 'conexion.js'
const promise = require('../models/conexion.js');

// Se importa el objeto de conexión a la base de datos
const conexion = require('../models/conexion.js');

// Se crea un objeto vacío que contendrá los métodos relacionados con los alumnos
var AlumnosDb = {};


// Se define el método 'insertar' en el objeto 'AlumnosDb'
AlumnosDb.insertar = function insertar(alumno){
    // Se retorna una promesa para realizar la inserción en la base de datos
    return new Promise((resolve,reject)=>{
        // Se define la consulta SQL para insertar el alumno
        var sqlConsulta = "insert into alumnos set ?";
        // Se ejecuta la consulta usando la conexión a la base de datos
        conexion.query(sqlConsulta,alumno,function(err,res){
            // Si se produce un error durante la ejecución de la consulta
            if(err){
                console.log('Surgio un error ' + err.message);
                // Se rechaza la promesa y se devuelve el error
                reject(err);
            } else {
                // Se resuelve la promesa y se devuelve un objeto con los datos del alumno insertado
                resolve({
                    id:res.insertId,
                    matricula:alumno.matricula,
                    nombre:alumno.nombre,
                    domicilio:alumno.domicilio,
                    sexo:alumno.sexo,
                    especialidad:alumno.especialidad
                });
            }
        });
  
    });
  }
  
  // Se define el método 'borrar' en el objeto 'AlumnosDb'
  AlumnosDb.borrar = function borrar(matricula){
    // Se retorna una promesa para realizar la inserción en la base de datos
    return new Promise ((resolve, reject)=>{
        var sqlConsulta = "delete from alumnos where matricula = ?";
        // Se ejecuta la consulta usando la conexión a la base de datos
        conexion.query(sqlConsulta,[matricula],function(err,res){
            if (err){
                console.log('Surgió un error ' + err.message);
                // Se rechaza la promesa y se devuelve el error
                reject(err);
            }else{
                resolve(res);
            }
        })
    })
  }

  // Se define el método 'actualizar' en el objeto 'AlumnosDb'
  AlumnosDb.actualizar = function actualizar(matricula, alumno){
    return new Promise((resolve, reject) => {
        var sqlConsulta = "UPDATE alumnos SET nombre = ?, domicilio = ?, sexo = ?, especialidad = ? WHERE matricula = ?";
        // Se ejecuta la consulta usando la conexión a la base de datos
        conexion.query(sqlConsulta, [alumno.nombre, alumno.domicilio, alumno.sexo, alumno.especialidad, matricula], function(err, res) {
            if(err) {
                console.log('Surgió un error ' + err.message);
                // Se rechaza la promesa y se devuelve el error
                reject(err);
            } else {
                console.log(res, matricula, alumno);
                resolve(res);
            }
        });
    });
}

// Se define el método 'buscar' en el objeto 'AlumnosDb'
AlumnosDb.buscarXMatricula = function buscarXMatricula(matricula){
    return new Promise((resolve, reject)=>{
        var sqlMostrar = "select * from alumnos where matricula = ?"
        // Se ejecuta la consulta usando la conexión a la base de datos
        conexion.query(sqlMostrar,[matricula],function(err,res){
            // Si se produce un error durante la ejecución de la consulta
            if(err){
                console.log('Surgio un error ' + err.message);
                // Se rechaza la promesa y se devuelve el error
                reject(err);
            } else {
                // Se muestra el resultado de la consulta como un objeto JSON
                console.log(res);
                resolve(res);
            }
        });
    })
}

// Se define el método 'mostrarTodos' en el objeto 'AlumnosDb'
AlumnosDb.mostrarTodos = function mostrarTodos(){
    // Se retorna una promesa para realizar la consulta de todos los alumnos
    return new Promise ((resolve, reject)=>{
        // Se define la consulta SQL para obtener todos los alumnos
        var sqlConsulta = "select * from alumnos";
        // Se ejecuta la consulta usando la conexión a la base de datos
        conexion.query(sqlConsulta,null,function(err,res){
            // Si se produce un error durante la ejecución de la consulta
            if (err){
                console.log('Surgio un error ' + err.message);
                // Se rechaza la promesa y se devuelve el error
                reject(err);
            }else {
                // Se muestra el resultado de la consulta como un objeto JSON
                //console.log(res.json);
                //console.log(res);
                resolve(res);
            }
        });
    });
}

module.exports = AlumnosDb;


